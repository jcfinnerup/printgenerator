###############################
###        TEST.py          ###
###############################


############################
###        Imports       ###
############################

# STL, Numpy
from PIL import Image
import math
import numpy as np
from pylab import imread
from scipy.misc import lena, imresize
from scipy.ndimage import gaussian_filter
from stl_tools import numpy2stl

 
 
############################
###      Variables       ###
############################

FIXED_RIGE_HEIGHT = -250
FIXED_BED_HEIGHT = -150
BUTTOM_HEIGHT = 1500
RIGE_THRESHOLD = 150


# Finger URL
fingerUrl = 'mold_cb_h_01'
desc = ''

# Importing Finger 
finger = Image.open('../Prints/Real/'+fingerUrl+'.jpg')
imgSeq = finger.getdata()
imgList = list(imgSeq)
imgTotalPix = len(imgList)
imgRes = math.sqrt(imgTotalPix)

# Loop through image to generate 2D matrix for STL
imgMatrix = [] 
imgMatrix.append
for row in range(0, int(imgRes)):
    temp = []
    for column in range(1, int(imgRes)):
        if row == 0 or row == int(imgRes)-1:
            temp.append(BUTTOM_HEIGHT)
        else:
            if column == 0 or column == int(imgRes)-1:
                temp.append(BUTTOM_HEIGHT)
            else:
                index = row*int(imgRes)+column
                pixel = imgList[index]
                if pixel >= RIGE_THRESHOLD:
                    temp.append(FIXED_BED_HEIGHT)
                else:
                    temp.append(FIXED_RIGE_HEIGHT)
    imgMatrix.append(temp)
    
# Generate STL from matrix
#A = imread('../Prints/Real/cb_01.jpg') # read from rendered png
A = np.matrix(imgMatrix)
A = gaussian_filter(A.max() - A, 2)
numpy2stl(A, '../Prints/Printed/'+desc+'diy'+fingerUrl+'.stl', scale=0.1, max_width=30,solid=True)
    
